## ConfigurationUpdater

Call this program with the method "SetValue", "GetValue", "SetValueOnConfiguration", "RestartNomadJob" or "Listen".

Call "SetValue" with "Key" and "Value".
                           
Call "GetValue" with "Key".

Call "SetValueOnConfiguration" with "Key", "TemplateConfigurationFile", "OutputConfigurationFile" and n-combinations of "ReplaceKey" and "ConsulKey".

Call "RestartNomadJob" with the "JobId".

Call "Listen" with "JobId", "Key", "TemplateConfigurationFile", "OutputConfigurationFile" and n-combinations of "ReplaceKey" and "ConsulKey".