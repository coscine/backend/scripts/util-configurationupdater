﻿using Consul;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationUpdater
{
    class Consul
    {

        public static async Task<string> SetValue(string key, string value)
        {
            using (var client = new ConsulClient())
            {
                var putPair = new KVPair(key)
                {
                    Value = Encoding.UTF8.GetBytes(value)
                };

                var putAttempt = await client.KV.Put(putPair);

                if (putAttempt.Response)
                {
                    var getPair = await client.KV.Get(key);
                    return Encoding.UTF8.GetString(getPair.Response.Value, 0,
                        getPair.Response.Value.Length);
                }
                return "An error occcurred while setting the key value pair: " + putAttempt.Response;
            }
        }

        public static async Task<string> GetValue(string key)
        {
            using (var client = new ConsulClient())
            {
                var getPair = await client.KV.Get(key);
                if (getPair.Response != null)
                {
                    return Encoding.UTF8.GetString(getPair.Response.Value, 0,
                        getPair.Response.Value.Length);
                } else
                {
                    return "An error occcurred while retrieving the key: " + key;
                }
            }
        }

        public static string TaskRetriever(Task<string> task)
        {
            task.Wait();
            return task.Result;
        }

        internal static void UpdateConfigurationFile(string templateConfigurationFile, string outputConfigurationFile, Dictionary<string, string> listenKeys, Dictionary<string, string> currentValues)
        {
            string text = File.ReadAllText(templateConfigurationFile);
            foreach (var kv in listenKeys)
            {
                text = text.Replace(kv.Value, currentValues[kv.Key]);
            }
            File.WriteAllText(outputConfigurationFile, text);
        }
    }
}
