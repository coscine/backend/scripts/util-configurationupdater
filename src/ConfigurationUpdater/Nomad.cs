﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationUpdater
{
    class Nomad
    {

        public static string UpdateJob(string jobId)
        {
            string jobUrl = "http://localhost:4646/v1/job/" + jobId;

            var request = (HttpWebRequest)WebRequest.Create(jobUrl);

            var response = (HttpWebResponse)request.GetResponse();

            var jobString = "";
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                jobString = streamReader.ReadToEnd();
            }

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(jobUrl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"Job\":" + jobString + "}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            string result = "";
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

    }
}
