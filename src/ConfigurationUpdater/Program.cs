using Consul;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationUpdater
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {

                if (args.Length > 0)
                {
                    switch (args[0])
                    {
                        case "SetValue":
                            if (args.Length == 3)
                            {
                                Console.WriteLine(Consul.TaskRetriever(Consul.SetValue(args[1], args[2])));
                            }
                            else
                            {
                                Console.WriteLine("Please call \"SetValue\" with \"ConsulKey\" and \"ConsulValue\"");
                            }
                            break;
                        case "GetValue":
                            if (args.Length == 2)
                            {
                                Console.WriteLine(Consul.TaskRetriever(Consul.GetValue(args[1])));
                            }
                            else
                            {
                                Console.WriteLine("Please call \"GetValue\" with \"ConsulKey\"");
                            }
                            break;
                        case "SetValueOnConfiguration":
                            if (args.Length >= 5)
                            {
                                string templateConfigFile = args[1];
                                string outputConfigFile = args[2];

                                Dictionary<string, string> listenKeys = new Dictionary<string, string>();
                                Dictionary<string, string> currentValues = new Dictionary<string, string>();
                                for (int i = 3; i < args.Length; i = i + 2)
                                {
                                    listenKeys.Add(args[i + 1], args[i]);
                                    currentValues.Add(args[i + 1], Consul.TaskRetriever(Consul.GetValue(args[i + 1])));
                                }

                                Consul.UpdateConfigurationFile(templateConfigFile, outputConfigFile, listenKeys, currentValues);
                                Console.WriteLine("Replaced \"" + string.Join(";", listenKeys.Select(x => x.Key + "=" + x.Value).ToArray()) + "\" with \"" + string.Join(";", currentValues.Select(x => x.Key + "=" + x.Value).ToArray()) + "\" in \"" + outputConfigFile + "\"");
                            }
                            else
                            {
                                Console.WriteLine("Please call \"SetValueOnConfiguration\" with \"TemplateConfigurationFile\", \"OutputConfigurationFile\" and n-combinations of \"ReplaceKey\" and \"ConsulKey\"");
                            }
                            break;
                        case "RestartNomadJob":
                            // Currently there is no Nomad .NET API
                            if (args.Length == 2)
                            {
                                string jobId = args[1];
                                Console.WriteLine(Nomad.UpdateJob(jobId));
                            }
                            else
                            {
                                Console.WriteLine("Please call \"RestartNomadJob\" with the \"JobId\"");
                            }
                            break;
                        case "Listen":
                            if(args.Length >= 6)
                            {
                                string jobId = args[1];
                                string templateConfigFile = args[2];
                                string outputConfigFile = args[3];
                                Dictionary<string, string> listenKeys = new Dictionary<string, string>();
                                Dictionary<string, string> currentValues = new Dictionary<string, string>();
                                for (int i = 4; i < args.Length; i = i + 2)
                                {
                                    listenKeys.Add(args[i + 1], args[i]);
                                    currentValues.Add(args[i + 1], Consul.TaskRetriever(Consul.GetValue(args[i + 1])));
                                }
                                Consul.UpdateConfigurationFile(templateConfigFile, outputConfigFile, listenKeys, currentValues);
                                Nomad.UpdateJob(jobId);
                                Console.WriteLine("Replaced \"" + string.Join(";", listenKeys.Select(x => x.Key + "=" + x.Value).ToArray()) + "\" with \"" + string.Join(";", currentValues.Select(x => x.Key + "=" + x.Value).ToArray()) + "\" in \"" + outputConfigFile + "\"");

                                while (true)
                                {
                                    // Checks every minute
                                    System.Threading.Thread.Sleep(60000);

                                    bool valueChange = false;
                                    foreach(var kv in listenKeys)
                                    {
                                        var currentValue = Consul.TaskRetriever(Consul.GetValue(kv.Key));
                                        if (currentValue != kv.Value)
                                        {
                                            currentValues[kv.Key] = currentValue;
                                            valueChange = true;
                                        }
                                    }

                                    if (valueChange)
                                    {
                                        Consul.UpdateConfigurationFile(templateConfigFile, outputConfigFile, listenKeys, currentValues);
                                        Nomad.UpdateJob(jobId);
                                        Console.WriteLine("Replaced \"" + string.Join(";", listenKeys.Select(x => x.Key + "=" + x.Value).ToArray()) + "\" with \"" + string.Join(";", currentValues.Select(x => x.Key + "=" + x.Value).ToArray()) + "\" in \"" + outputConfigFile + "\"");
                                    }
                                }

                            } 
                            else
                            {
                                Console.WriteLine("Please call \"Listen\" with \"JobId\", \"TemplateConfigurationFile\", \"OutputConfigurationFile\" and n-combinations of \"ReplaceKey\" and \"ConsulKey\"");
                            }
                            break;
                        default:
                            Console.WriteLine("Please call this program with the method \"SetValue\", \"GetValue\", \"SetValueOnConfiguration\", \"RestartNomadJob\" or \"Listen\"");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Please call this program with the method \"SetValue\", \"GetValue\", \"SetValueOnConfiguration\" or \"RestartNomadJob\"");
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred during execution: " + e.Message);
            }
            
        }

        
    }
}
